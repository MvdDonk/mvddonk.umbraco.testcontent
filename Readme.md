[![Build status](https://ci.appveyor.com/api/projects/status/a4d3koxqeyttrvbh?svg=true)](https://ci.appveyor.com/project/MvdDonk/mvddonk-umbraco-testcontent)
[![Documentation Status](https://readthedocs.org/projects/umbracotestcontent/badge/?version=master)](http://umbracotestcontent.readthedocs.org/en/master/)
[![NuGet release](https://img.shields.io/nuget/v/MvdDonk.Umbraco.TestContent.svg)](https://www.nuget.org/packages/MvdDonk.Umbraco.TestContent/)

# ![Umbraco Test Content](https://bytebucket.org/MvdDonk/mvddonk.umbraco.testcontent/raw/959990421f4a751fd903272806db03115ebd89fc/docs/icon.png "Umbraco Test Content") Umbraco Test Content #
Create multiple pages with realistic test content by just using the Umbraco interface.

1. Pick a parent node and use the context menu to start creating testcontent.
2. Select the document type you want to use.
3. For each property you want to use choose either:
    - Manually enter a value just like normal, no random generation will be used.
    - Enable a contentcreator by turning on the switch and selecting a generator in the dropdown.
4. At the bottom select how many items you would like to create.
5. Also at the bottom you can choose which language to use when generating values (by using any of the contentcreators).
6. Click the 'Generate and publish' or 'Generate' button to start creating testcontent!

Creating test content using the Umbraco interface:
![Create dialog](https://bytebucket.org/MvdDonk/mvddonk.umbraco.testcontent/raw/d8f2b36ef79321afda0b0a87af90ca43972afe54/docs/CreateDialog.png "Create test content dialog")

Selecting content creators for fields and entering how many pages we want:
![Create multiple test pages](https://bytebucket.org/MvdDonk/mvddonk.umbraco.testcontent/raw/d8f2b36ef79321afda0b0a87af90ca43972afe54/docs/CreateMultiplePages.png "Create multiple test content pages")

Generated content:
![Generated content](https://bytebucket.org/MvdDonk/mvddonk.umbraco.testcontent/raw/7872a115b667dfaad48d81f64f3f8117f1c077f9/docs/Generated%20content.png "Generated content")

## Documentation ##
For the full documentation please go to [http://umbracotestcontent.readthedocs.org/en/master/](http://umbracotestcontent.readthedocs.org/en/master/)

