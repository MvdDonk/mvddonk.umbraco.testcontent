﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Web.PropertyEditors.ValueConverters;
using Core = Umbraco.Core;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Square image", "aad8fff4-2aa8-4879-9b03-71eeab71c40b", "Umbraco.MediaPicker2", SortOrder = 30)]
    public class SquareMediaCreator : BaseMediaCreator
    {
        protected override int ImageWidth => 400;
        protected override int ImageHeight => 400;
        protected override string MediaName => "Portrait-test-media";
    }
}
