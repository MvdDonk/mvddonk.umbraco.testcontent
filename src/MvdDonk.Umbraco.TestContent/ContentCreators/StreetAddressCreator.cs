﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Address with street", "245a1385-a7f9-4dc1-808a-6649d4e72dee", "Umbraco.Textbox", SortOrder = 30)]
    public class StreetAddressCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Bogus.Faker(locale);
            return faker.Address.StreetAddress(false);
        }
    }
}
