﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Multiple lorem ipsum paragraphs", "b2ce5ed6-15e0-44a0-a2fd-4772fd65b5f9", "Umbraco.TinyMCEv3", SortOrder = 20)]
    public class RteLongLoremIpsumCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return string.Join("\n", faker.Lorem.Paragraph(10), faker.Lorem.Paragraph(10), faker.Lorem.Paragraph(10));
        }
    }
}
