﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Domain name", "9d1f1356-ba2e-4cb9-8735-2487b72e6805", "Umbraco.Textbox", SortOrder = 60)]
    public class DomainNameCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Internet.DomainName();
        }
    }
}
