﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Date in the coming week", "f7a05e43-c0b5-422e-832a-09760c83e1b1", "Umbraco.Date", SortOrder = 100)]
    public class DateSoonCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Date.Soon(7);
        }
    }
}
