﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Web.PropertyEditors.ValueConverters;
using Core = Umbraco.Core;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Portrait image", "e9462b91-03ba-46ee-bcc4-d975abe50daa", "Umbraco.MediaPicker2", SortOrder = 20)]
    public class PortraitMediaCreator : BaseMediaCreator
    {
        protected override int ImageWidth => 400;
        protected override int ImageHeight => 600;
        protected override string MediaName => "Portrait-test-media";
    }
}
