﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Short lorem ipsum", "045362fd-e95d-4e73-ab44-5fa34ba951cc", "Umbraco.Textbox", SortOrder = 10)]
    public class ShortLoremIpsumCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Lorem.Sentence(5);
        }
    }
}
