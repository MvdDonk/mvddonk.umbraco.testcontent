﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Number", "06793f02-9e65-4a4f-8035-6e2c70b97eb4", "Umbraco.Integer", SortOrder = 10)]
    public class NumberCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Random.Number();
        }
    }
}
