﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Email", "55866008-1f9e-435b-86ac-ddcded264c87", "Umbraco.Textbox", SortOrder = 80)]
    public class EmailCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Internet.Email();
        }
    }
}
