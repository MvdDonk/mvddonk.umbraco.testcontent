﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    public class ContentCreatorAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Id { get; private set; }
        public int SortOrder { get; set; }
        public string[] SupportedEditors { get; private set; }

        public ContentCreatorAttribute(string name, string id, params string[] supportedEditors)
        {
            Name = name;
            Id = id;
            SortOrder = 100;
            SupportedEditors = supportedEditors;
        }
    }
}
