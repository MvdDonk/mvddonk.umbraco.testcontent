﻿using Bogus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Web.PropertyEditors.ValueConverters;
using Core = Umbraco.Core;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    public abstract class BaseMediaCreator : IContentCreator
    {
        protected abstract int ImageWidth { get; }
        protected abstract int ImageHeight { get; }
        protected abstract string MediaName { get; }

        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            var imageUrl = faker.Image.Image(ImageWidth, ImageHeight, true, true);

            var tempFile = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid()}.jpg");
            using (var client = new WebClient())
            {
                client.DownloadFile(imageUrl, tempFile);
            }

            var mediaTestContentContainer = Core.ApplicationContext.Current.Services.MediaService.GetRootMedia().FirstOrDefault(m => m.Name == "Test Content");
            if(mediaTestContentContainer == null)
            {
                mediaTestContentContainer = Core.ApplicationContext.Current.Services.MediaService.CreateMediaWithIdentity("Test Content", -1, Core.Constants.Conventions.MediaTypes.Folder);
                Core.ApplicationContext.Current.Services.MediaService.Save(mediaTestContentContainer);
            }

            var fileName = $"{MediaName}.jpg";
            var image = Core.ApplicationContext.Current.Services.MediaService.CreateMediaWithIdentity(fileName, mediaTestContentContainer, Core.Constants.Conventions.MediaTypes.Image);
            image.SetValue(Core.Constants.Conventions.Media.File, fileName, System.IO.File.OpenRead(tempFile));
            Core.ApplicationContext.Current.Services.MediaService.Save(image);

            return image.Id;
        }
    }
}
