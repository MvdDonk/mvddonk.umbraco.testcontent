﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    public interface IContentCreator
    {
        object CreateContent(string locale = "en");
    }
}