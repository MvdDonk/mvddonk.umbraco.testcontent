﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Long lorem ipsum", "abadd4c6-87ee-4c95-b48b-10e7a813eb96", "Umbraco.Textbox", "Umbraco.TextboxMultiple", SortOrder = 20)]
    public class LongLoremIpsumCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Lorem.Sentence(10);
        }
    }
}
