﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Date in the future", "415f153f-4045-46dd-98b0-1a07b6811af8", "Umbraco.Date", SortOrder = 150)]
    public class DateFutureCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Date.Future();
        }
    }
}
