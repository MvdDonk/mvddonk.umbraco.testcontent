﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Full name", "ef822901-47c9-4747-9ae4-27d2b5a0959f", "Umbraco.Textbox", SortOrder = 50)]
    public class FullNameCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Name.FullName();
        }
    }
}
