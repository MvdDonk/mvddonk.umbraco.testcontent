﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Single lorem ipsum paragraph", "2c2160da-3661-457a-96cb-a3c5c766ce58", "Umbraco.TinyMCEv3", SortOrder = 10)]
    public class RteShortLoremIpsumCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Lorem.Paragraph(10);
        }
    }
}
