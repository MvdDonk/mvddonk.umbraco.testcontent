﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Web.PropertyEditors.ValueConverters;
using Core = Umbraco.Core;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Landscape image", "a64a3d97-60fa-4e1d-b35a-3ec9d596c20c", "Umbraco.MediaPicker2", SortOrder = 10)]
    public class LandscapeMediaCreator : BaseMediaCreator
    {
        protected override int ImageWidth => 640;
        protected override int ImageHeight => 480;
        protected override string MediaName => "Landscape-test-media";
    }
}
