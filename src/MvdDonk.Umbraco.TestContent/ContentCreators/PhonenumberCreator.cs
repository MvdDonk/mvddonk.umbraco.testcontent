﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Phonenumber", "e8a8c0c3-d340-4584-aeaf-311b2c2ba740", "Umbraco.Textbox", SortOrder = 40)]
    public class PhonenumberCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Phone.PhoneNumber();
        }
    }
}
