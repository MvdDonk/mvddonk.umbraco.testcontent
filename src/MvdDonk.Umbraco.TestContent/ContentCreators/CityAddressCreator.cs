﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("City", "2530e303-f327-4e19-9e5a-eec197d273c7", "Umbraco.Textbox", SortOrder = 50)]
    public class CityAddressCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Address.City();
        }
    }
}
