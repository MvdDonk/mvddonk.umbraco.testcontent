﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Date in the past (20+ years)", "b331cdcd-74ff-4fcc-bef2-e1562d769973", "Umbraco.Date", SortOrder = 250)]
    public class DateVeryPastCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Date.Past(refDate: DateTime.Now.AddYears(-20));
        }
    }
}
