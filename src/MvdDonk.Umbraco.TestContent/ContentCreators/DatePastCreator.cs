﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.ContentCreators
{
    [ContentCreator("Date in the past", "a5d899ad-2a90-49b9-95ad-6ddb3c4fc7ed", "Umbraco.Date", SortOrder = 200)]
    public class DatePastCreator : IContentCreator
    {
        public object CreateContent(string locale = "en")
        {
            var faker = new Faker(locale);
            return faker.Date.Past();
        }
    }
}
