﻿using MvdDonk.Umbraco.TestContent.ContentCreators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent
{
    internal static class ContentCreatorsManager
    {
        internal class ContentCreatorInfo
        {
            public IContentCreator ContentCreator { get; set; }
            public string[] SupportedEditors { get; set; }
            public string Name { get; set; }
            public int SortOrder { get; set; }
        }
        
        internal static Dictionary<string, ContentCreatorInfo> AllContentCreatorsLookup = new Dictionary<string, ContentCreatorInfo>();

        private static object _lock = new object();

        internal static IContentCreator GetContentCreatorById(string id)
        {
            ContentCreatorInfo creatorInfo = null;
            if(string.IsNullOrEmpty(id) || AllContentCreatorsLookup.TryGetValue(id, out creatorInfo))
            {
                return creatorInfo.ContentCreator;
            }

            return null;
        }
      
        internal static void FindAllContentCreators()
        {
            if (AllContentCreatorsLookup.Count > 0)
            {
                return;
            }

            lock (_lock)
            {
                if (AllContentCreatorsLookup.Count > 0)
                {
                    return;
                }

                var typeToCheck = typeof(IContentCreator);
                var types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => typeToCheck.IsAssignableFrom(p));

                foreach (var type in types.Where(t => !t.IsInterface))
                {
                    var contentCreatorAttribute = type.GetCustomAttributes(typeof(ContentCreatorAttribute), false).FirstOrDefault() as ContentCreatorAttribute;
                    if (contentCreatorAttribute == null || !contentCreatorAttribute.SupportedEditors.Any())
                    {
                        continue;
                    }

                    var instance = (IContentCreator)Activator.CreateInstance(type);
                    var info = new ContentCreatorInfo
                    {
                        ContentCreator = instance,
                        SupportedEditors = contentCreatorAttribute.SupportedEditors,
                        Name = contentCreatorAttribute.Name,
                        SortOrder = contentCreatorAttribute.SortOrder
                    };

                    AllContentCreatorsLookup.Add(contentCreatorAttribute.Id, info);
                }
            }
        }
    }
}
