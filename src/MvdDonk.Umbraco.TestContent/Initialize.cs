﻿using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using System.Web;
using Umbraco.Core.Security;
using Core = Umbraco.Core;
using System;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Configuration;
using umbraco;

namespace MvdDonk.Umbraco.TestContent
{
    public class Initialize : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            TreeControllerBase.MenuRendering += ContentTreeController_MenuRendering;
            ContentCreatorsManager.FindAllContentCreators();

            RouteTable.Routes.MapRoute(
               name: "MvdDonk.Umbraco.TestContent.UmbracoContent",
               url: GlobalSettings.Path.TrimStart("/") + "/views/MvdDonk.Umbraco.TestContent/{action}.html",
               defaults: new { controller = "UmbracoContent", action = "Index", id = UrlParameter.Optional }
           );
        }

        private void ContentTreeController_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (e.NodeId == "-1")
            {
                return;
            }

            var userId = sender.User.Identity.GetUserId<int>();
            var allowedSections = sender.ApplicationContext.Services.SectionService.GetAllowedSections(userId);
            //var backOfficeUserManager = HttpContext.Current.GetOwinContext().GetBackOfficeUserManager();

            //Check if user has developer section access
            if (!allowedSections.Any(s => s.Alias == Core.Constants.Applications.Developer))
            {
                return;
            }

            switch (sender.TreeAlias)
            {
                case Core.Constants.Trees.Content:
                    var i = new MenuItem("createTestContent", "Create test content");
                    i.AdditionalData.Add("actionView", "/App_Plugins/MvdDonk.Umbraco.TestContent/CreateTestContentDialog.html");
                    i.Icon = "sandbox-toys";
                    e.Menu.Items.Insert(Math.Min(e.Menu.Items.Count, 3), i);
                    break;
            }
        }
    }
}
