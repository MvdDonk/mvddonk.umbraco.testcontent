﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.Models
{
    public class CreateModel
    {
        [JsonProperty(PropertyName = "parentId")]
        public int ParentId { get; set; }

        [JsonProperty(PropertyName = "publishNodes")]
        public bool PublishNodes { get; set; }

        [JsonProperty(PropertyName = "nodeNameProperty")]
        public Property NodeNameProperty { get; set; }

        [JsonProperty(PropertyName = "numberOfTestItems")]
        public int NumberOfTestItems { get; set; }

        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "contentTypeAlias")]
        public string ContentTypeAlias { get; set; }

        [JsonProperty(PropertyName = "properties")]
        public List<Property> Properties { get; set; }

        public class Property
        {
            [JsonProperty(PropertyName = "id")]
            public int Id { get; set; }
            [JsonProperty(PropertyName = "alias")]
            public string Alias { get; set; }
            [JsonProperty(PropertyName = "defaultValue")]
            public string DefaultValue { get; set; }
            [JsonProperty(PropertyName = "generateType")]
            public string GenerateType { get; set; }
        }
    }
}
