﻿using MvdDonk.Umbraco.TestContent.ContentCreators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvdDonk.Umbraco.TestContent.Models
{
    public class ContentCreatorDTO
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "supportedEditors")]
        public string[] SupportedEditors { get; set; }

        [JsonProperty(PropertyName = "sortOrder")]
        public int SortOrder { get; set; }
    }
}