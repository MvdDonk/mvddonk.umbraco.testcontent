﻿using MvdDonk.Umbraco.TestContent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace MvdDonk.Umbraco.TestContent.Controllers
{
    [PluginController("TestContent")]
    public class CreateTestContentApiController : UmbracoAuthorizedApiController// UmbracoAuthorizedJsonController
    {
        [HttpGet]
        public IHttpActionResult ContentCreators()
        {
            return Ok(ContentCreatorsManager.AllContentCreatorsLookup.Select(c => new ContentCreatorDTO
            {
                Id = c.Key,
                Name = c.Value.Name,
                SupportedEditors = c.Value.SupportedEditors,
                SortOrder = c.Value.SortOrder
            }).ToList());
        }

        [HttpPost]
        public IHttpActionResult Create(CreateModel model)
        {
            string locale = model.Language ?? "en";
            int? firstNodeId = null;
            for (int i = 0; i < model.NumberOfTestItems; i++)
            {
                object nodeNameValue = model.NodeNameProperty.DefaultValue;
                if (!string.IsNullOrEmpty(model.NodeNameProperty.GenerateType))
                {
                    var creator = ContentCreatorsManager.GetContentCreatorById(model.NodeNameProperty.GenerateType);
                    nodeNameValue = creator.CreateContent(locale);
                }

                var content = ApplicationContext.Services.ContentService.CreateContent(nodeNameValue.ToString(), model.ParentId, model.ContentTypeAlias);
                foreach (var property in model.Properties)
                {
                    if (!content.Properties.Contains(property.Alias))
                    {
                        continue;
                    }

                    var nodeProperty = content.Properties[property.Alias];
                    if (nodeProperty == null)
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(property.GenerateType))
                    {
                        nodeProperty.Value = property.DefaultValue;
                    }
                    else
                    {
                        var contentCreator = ContentCreatorsManager.GetContentCreatorById(property.GenerateType);
                        if (contentCreator != null)
                        {
                            nodeProperty.Value = contentCreator.CreateContent(locale);
                        }
                        else
                        {
                            nodeProperty.Value = property.DefaultValue;
                        }
                    }
                }
                if (model.PublishNodes)
                {
                    ApplicationContext.Services.ContentService.SaveAndPublishWithStatus(content);
                }
                else
                {
                    ApplicationContext.Services.ContentService.Save(content);
                }

                if (!firstNodeId.HasValue)
                {
                    firstNodeId = content.Id;
                }
            }

            return Ok(firstNodeId);
        }
    }
}
