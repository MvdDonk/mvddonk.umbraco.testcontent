﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvdDonk.Umbraco.TestContent.Controllers
{
    public class UmbracoContentController : Controller
    {
        public ActionResult CreateTestContent()
        {
            return File("/App_Plugins/MvdDonk.Umbraco.TestContent/CreateTestContent.html", "text /html");
        }
    }
}
