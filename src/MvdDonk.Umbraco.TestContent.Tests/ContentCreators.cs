﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvdDonk.Umbraco.TestContent.ContentCreators;

namespace MvdDonk.Umbraco.TestContent.Tests
{
    [TestClass]
    public class ContentCreators : BaseTest
    {
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
            ContentCreatorsManager.FindAllContentCreators();
        }

        [TestMethod, TestCategory("Content creators except media")]
        public void AllContentCreators()
        {
            foreach (var contentCreatorInfo in ContentCreatorsManager.AllContentCreatorsLookup.Values)
            {
                if (!typeof(BaseMediaCreator).IsAssignableFrom(contentCreatorInfo.ContentCreator.GetType()))
                {
                    TestSingleContentCreator(contentCreatorInfo.ContentCreator, contentCreatorInfo.Name);
                }
            }
        }

        private void TestSingleContentCreator(IContentCreator creator, string name)
        {
            var value = creator.CreateContent();
            WriteOutput(value, name);
            Assert.IsFalse(value == null, name);
        }
    }
}
