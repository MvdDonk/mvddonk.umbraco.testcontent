﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvdDonk.Umbraco.TestContent.ContentCreators;

namespace MvdDonk.Umbraco.TestContent.Tests
{
    public abstract class BaseTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        protected void WriteOutput(object value, string name)
        {
            const int blockLength = 80;

            WriteTextLine(blockLength, name, '-', '-');
            var line = "No value found";
            if (value != null)
            {
                line = value.ToString();
            }

            if (line.Length - 6 > blockLength)
            {
                line = line.Substring(0, blockLength - 6) + "..";
            }

            WriteTextLine(blockLength, line);
            TestContext.WriteLine(new string('-', blockLength));
            TestContext.WriteLine("");
        }

        private void WriteTextLine(int blockLength, string line, char filler = ' ', char caps = '|')
        {
            line = $" {line} ";
            var left = (int)Math.Floor((blockLength - line.Length - 2) / 2D);
            var right = (int)Math.Ceiling((blockLength - line.Length - 2) / 2D);
            TestContext.WriteLine($"{caps}{new string(filler, left)}{line}{new string(filler, right)}{caps}");
        }
    }
}
