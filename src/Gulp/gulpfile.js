﻿/// <binding ProjectOpened='watch' />
'use strict';

var gulp = require('gulp');

var paths = {
    app_plugins: ['../MvdDonk.Umbraco.TestContent/App_Plugins/**/*.*']
};

gulp.task('app_plugins',
    function () {
        gulp.src(paths.app_plugins)
            .pipe(gulp.dest('../MvdDonk.Umbraco.TestContent.Site/App_Plugins/'));
    });

gulp.task('watch', ['app_plugins'], function () {
    gulp.watch(paths.app_plugins, ['app_plugins']);
});

